
# Addressbook API


## Development

Make sure you are using

- NODE 8.11.1+
- NPM 5.6.0+
- DOCKER (18+)

Download repository via git 
```sh
  git clone https://MiriKabrt@bitbucket.org/MiriKabrt/strv-addressbook.git
```
Go to the project folder
```sh
  cd strv-addressbook
```

Install packages
```sh
  npm i
```
Install environments via docker
```sh
  npm run infrastructure:start
```
Run it for development
```sh
  npm run dev
```

Dont forget create file '.env' with these environment variables
```sh
  FIREBASE_API_KEY
  FIREBASE_MSG_ID
```


## Deployment

Just push your code to bitbucket master branch and circleCI will deploy API to heroku development app


## Tests

```sh
  npm run test
```

or

```sh
  npm run cover
```