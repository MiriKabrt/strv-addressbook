'use strict'

const firebase = require('firebase/app')
const config = require('../config')

require('firebase/database')

firebase.initializeApp(config.firebase)

module.exports = firebase
