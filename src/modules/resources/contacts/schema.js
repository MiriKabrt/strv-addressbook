'use strict'

const joi = require('joi')
const config = require('../../config')

module.exports = {

  create: joi.object().keys({
    email: joi.string().email().required()
      .max(config.validation.shortTextLength),
    firstName: joi.string().required().max(config.validation.shortTextLength),
    lastName: joi.string().required().max(config.validation.shortTextLength),
    phone: joi.string().min(9).required(),
    address: joi.string().required(),
  }),


}
