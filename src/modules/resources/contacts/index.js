'use strict'

const log = require('../../logging')
const firebase = require('../../firebase')
const schema = require('./schema')

module.exports = {

  schema,

  async create(user, contactData) {
    log.info({ email: contactData.email }, 'Create new contact.')

    const rootRef = await firebase.database().ref('contacts/').child(`user-${user.userId}`)
    rootRef.push(contactData)

    return {
      rootRef,
    }
  },
}
