'use strict'

const compose = require('koa-compose')
const middleware = require('../middleware')
const users = require('../../modules/resources/users')

module.exports = {

  post: compose([
    middleware.validator.validaBody(users.schema.register),
    async ctx => {
      // Perform registration
      const userData = ctx.request.body
      const registration = await users.register(userData)

      // Write response
      ctx.set('Authorization', registration.accessToken)
      ctx.body = registration.user
      ctx.status = 201
    },
  ]),

}
