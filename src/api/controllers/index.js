'use strict'

const users = require('./users')
const sessions = require('./sessions')
const contacts = require('./contacts')

module.exports = {
  users,
  sessions,
  contacts,
}
