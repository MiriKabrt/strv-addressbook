'use strict'

const compose = require('koa-compose')
const jwt = require('jsonwebtoken')
const middleware = require('../middleware')
const contacts = require('../../modules/resources/contacts')


module.exports = {

  post: compose([
    middleware.validator.validaBody(contacts.schema.create),
    middleware.secure.auth(),
    async ctx => {
      // Perform registration
      const contactData = ctx.request.body
      const user = jwt.decode(ctx.get('Authorization'))
      await contacts.create(user, contactData)

      ctx.body = contactData
      ctx.status = 201
    },
  ]),

}
