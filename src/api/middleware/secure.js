'use strict'

const auth = require('../../modules/auth')
const errors = require('../../modules/errors')
const log = require('../../modules/logging')

module.exports = {

  auth() {
    return async (ctx, nextMiddleware) => {
      log.info(ctx.get('Authorization'))
      if (!ctx.get('Authorization')) {
        throw new errors.Unauthorized()
      }
      await auth.verifyAccessToken(ctx.get('Authorization'))
      await nextMiddleware()
    }
  },
}
