'use strict'

const errors = require('./errors')
const validator = require('./validator')
const docs = require('./docs')
const secure = require('./secure')


module.exports = {
  errors,
  validator,
  docs,
  secure,
}
