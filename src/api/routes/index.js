'use strict'

const Router = require('koa-router')
const controllers = require('../controllers')

const router = new Router()

// Sessions
router.post('/sessions', controllers.sessions.post)

// Users
router.post('/users', controllers.users.post)

// Contacts
router.post('/contacts', controllers.contacts.post)


const middleware = router.routes()

module.exports = middleware
