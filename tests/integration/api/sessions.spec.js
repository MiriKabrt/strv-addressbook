'use strict'

const _ = require('lodash')
const request = require('supertest-koa-agent')
const db = require('../../../src/modules/database')
const users = require('../../../src/modules/resources/users')
const fake = require('../../common/fake-data')
const { expect } = require('../../common/chai')
const api = require('../../../src/api')


describe('POST /sessions', () => {

  beforeEach(() => db.init(true))

  context('Session', () => {

    it('Should create session', async () => {
      const fakeUser = fake.user()
      const expectedResult = _.omit(fakeUser, 'password')
      await users.register(fakeUser)
      const loginFakeUser = _.omit(fakeUser, 'firstName', 'lastName')
      const response = await request(api)
        .post('/sessions')
        .send(loginFakeUser)
        .expect(200)

      expect(response.body).to.shallowDeepEqual(expectedResult)
      expect(response.body).to.include.keys(['id', 'createdAt', 'updatedAt'])

      expect(response.headers).to.have.property('authorization')
      expect(response.headers.authorization).to.be.a('string')
    })


  })

})
